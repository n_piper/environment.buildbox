# Environment - BuildBox 

Environment creation of a central buildbox to manage continuous integration, automated builds & deploys.

For deploy & release automation an approach to direct validated SNAPSHOT & Release dependencies to create & register release & environment baselines.

Thanks for the packer scripting goes to shiguredo-packer-templates.


## OBSOLETE : No longer maintained

As an individual instance was no longer part of a cohesive build pipeline, this will no longer be maintained.

It has been superseded by [environment.buildfarm](https://bitbucket.org/n_piper/environment.buildfarm).

# Proposed Features / Capabilities

## Connectivity Requirments

* Buildbox is only authenticated agent who can create new Snapshot & Release repos in environemnt.repository
* maven to be installed, settings.xml has repository config settings for artifact creation.
* Can connect to external repos like github, bitbucket to create tags etc; as part of mvn:release process
* Ideally an aliased URL / statically named like 'buildbox.solveapuzzledev'
* Supports multiple usages CI, release process
* Box operates on principle it can be rebuilt & destroyed & recover config.
* Add keys & plugins via scripting & api-calls not through gui.

## Future options
* Deploy a provisioned instance to AWS?

## Storage requirements
* The path for storing the configuration should be persistent, if possible recover from a storage facility / public repo (Dropbox, AWS, github, bitbucket,..)
* Goal to limit build depth per job
* config should persist to 'vagrant_data'
* If vbox, VM snapshots are required storage growth may increase

## Security
* Public/Private keys - effort vs. management?
* Integration to Vault / Keystore?
* Only an Admin (vagrant?) & read only user to start

# Proposed implementation
* Packer
* Vagrant
* Puppet to install
* Recover storage backup as repository?

## Software installs
* Java OpenJDK
* Maven
* git
* puppet 
* nodeJS (not installed yet)
* Docker?? (not installed yet)
* Vagrant?? (not installed yet)
* Packer (not installed yet)
* Serverspec (not installed yet)

# Current capabilities

## v0.1

Packer & vagrant pipeline to provision & start automatically a jenkins server capable of building the following project types:

* Git repository
* Maven 3+/ Shell based builds
* Java - JDK v1.7

### Constraints
No security, email notification not configured, not integrated to a repository for snapshot / release option.

# To build
```
$ packer build -only=virtualbox-iso template.json
$ vagrant box add ubuntu14 ubuntu-14-04-x64-virtualbox.box  
$ vagrant up
```

# To test
http://localhost:8888

# References
* [MultiMachine vagrant](https://www.vagrantup.com/docs/multi-machine/)
* [Rtyler Jenkins Puppet](https://forge.puppetlabs.com/rtyler/jenkins)
* [Build, Automate server image creation](http://code.hootsuite.com/build-test-and-automate-server-image-creation/)
* [Configure Jenkins to use Gmail for SMTP](https://www.safaribooksonline.com/library/view/jenkins-the-definitive/9781449311155/ch04s08.html)
* [Testing puppet modules - vagrant , serverspec](https://engineering.opendns.com/2014/11/13/testing-puppet-modules-vagrant-serverspec/)
* [ServerSpec](http://serverspec.org/)
